---
title: About me
type: page
---


Soc programador full-stack, cursant actualment CFGS de programació (DAW), apassionat de la tecnologia i els Ordinadors. Estic Especialitzat en disseny, creació i manteniment web per a particulars i empreses. Nascut a Terrassa (Espanya).


![Currículum](/portfolio/aboutme/jo.jpg)