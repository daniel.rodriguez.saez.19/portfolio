---
title: Craft Made
type: page
---

### Craft Made

Últim projecte d'ABP, que consisteix en una aplicació web basada en un marketplace. La pàgina consisteix en una botiga de productes artesanals, on un artesà pot pujar els seus productes, per poder oferir-los. La creació d'aquesta web, va orientada, pel petit artesà, que no sap com oferir els seus productes.
Finalment, l'aplicació consumeix un Api Rest, que s'encarrega crear imatges, emmagatzemar i eliminar, al seu servidor, totes les fotografies que consumeix el marketplace.

#### Tecnologies empleades 

* Laravel
* Php
* JavaScript
* MariaDb
* Html
* Css
* Api Rest
* Sass

#### Enllaç Repositori 
[Repositori](https://github.com/jcadafalch/Marketplace)

##### Pantalla de login
![Projects](/portfolio/projects/login.png)


##### Pantalla nou usuari
![Projects](/portfolio/projects/nouUsuariCraftmade.png)



##### Pantalla de Home
![Projects](/portfolio/projects/homeMarketPlace.png)

##### Pantalla Landing Page
![Projects](/portfolio/projects/landingPage.png)


##### Pantalla detall de producte
![Projects](/portfolio/projects/detallProducte.png)


##### Pantalla perfil 
![Projects](/portfolio/projects/perfil.png)

##### Pantalla la meva tenda
![Projects](/portfolio/projects/tenda1.png)
![Projects](/portfolio/projects/tenda2.png)

##### Pantalla editar tenda
![Projects](/portfolio/projects/edittenda1.png)
![Projects](/portfolio/projects/edittenda2.png)

##### Pantalla carret
![Projects](/portfolio/projects/carrito.png)

##### Pantalla resum comanda
![Projects](/portfolio/projects/comanda.png)

##### Pdf comanda
![Projects](/portfolio/projects/pdf.png)

