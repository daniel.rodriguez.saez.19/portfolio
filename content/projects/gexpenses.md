---
title: GExpenses
type: page
---

### GExpenses

Primer projecte d'ABP, que consisteix en una aplicació web per creació d'activitat, de manera que pots portar un registre de les despeses, qui ha pagat, hi ha qui s'ha de pagar, en resum et facilita els comptes de qualsevol activitat.

#### Tecnologies empleades 

* Php
* JavaScript
* MariaDb
* Html
* Css

#### Enllaç Repositori 
[Repositori](https://git.copernic.cat/cadafalch.miro.jaume/gexpenses)

##### Pantalla nou usuari
![Projects](/portfolio/projects/pantalla1.png)
![Projects](/portfolio/projects/nouUsuari.png)

##### Pantalla de login
![Projects](/portfolio/projects/pantalla1.png)
![Projects](/portfolio/projects/pantalla2.png)


##### Pantalla de Home
![Projects](/portfolio/projects/home.png)

##### Pantalla Invitacions
![Projects](/portfolio/projects/invitacions.png)


##### Pantalla detall Activitat i pagament Basic
![Projects](/portfolio/projects/detallActivitat.png)


##### Pantalla detall Activitat i pagament per Imports
![Projects](/portfolio/projects/detallPagamentImports.png)

##### Pantalla detall Activitat i pagament per Parts
![Projects](/portfolio/projects/detallPagamentParts.png)
