---
title: Heroes of Vannaria 
type: page
---

#### **Projecte final d'1 de DAW.**
Projecte de Java enfocat a POO.
El projecte és vasa amb un joc de rol amb atacs per torns, amb la possibilitat de crear nous personatges.
Tens diferents tipus de classes i d'armes a escollir, cada classe amb els seus estats propis i les armes iguals.
La finalitat d'aquest projecte és agafar els fonaments de treballar amb classes i objectes.
Tot l'entorn és des de consola.

### Pantalla de Inici
![Projects](/portfolio/projects/Menu.png)

### Crear Personatge
![Projects](/portfolio/projects/CrearPersonatge.png)

### Modificar Personatge
![Projects](/portfolio/projects/ModificarPersonatge.png)

### Combat
![Projects](/portfolio/projects/Batalla1.png)
![Projects](/portfolio/projects/BatallaF.png)

### Mostrar Personatges
![Projects](/portfolio/projects/MostrarPersonatge.png)


[Repositori](https://gitlab.com/daniel.rodriguez.saez.19/HeroesOfVannaria)

