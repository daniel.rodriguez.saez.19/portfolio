---
title: Frase Secreta
type: page
---

### Java FX Frase Secreta 

Aplicació programada completament amb Java Fx.
Projecte amb el qual hem desenvolupat un joc, en el qual has d'endevinar la frase que està encriptada, amb l'opció d'obtenir 3 pistes.
Has d'introduir a cada combo box una lletra la qual correspondrà amb el número del combo box el número que apareix a la frase encriptada.

#### Pantalla inicial
![Projects](/portfolio/projects/javafx.png)

#### Pantall Partida
![Projects](/portfolio/projects/Pantallapartida.png)


#### Enllaç Repositori 
[Repositori](https://git.copernic.cat/rodriguez.saez.daniel/frase_secreta)