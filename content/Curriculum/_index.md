---
title: "Mi  perfil profesional"
type: page
---

## Índice de contenidos
* [Currículum](#curriculum)
* [Experiencia](#experiencia)




### Curriculum

![Currículum](/portfolio/curriculum/Curriculum.jpg)


### Experiencia


#### **2008-2021**

##### **Turbo Diésel Barbera**

Oficial de 1 

Especialista en inyección diésel, reparación de equipos de inyección, reparación y mantenimiento de vehículos, diagnosis y seguimiento de averías, interpretación de planos eléctricos.


#### 2022 - Actualidad

##### **SMILICS Technologies**

Formacíon profesinal Fp Dual en Smilics Technologies. 

Puesto de Full Stack Developer, trabajando con teclonogias: JavaScript,Java, Php Postman, Jmter, Spring Boot, Sonarqube, HTML, Css, PostgreSQL, MariaDB.

* Desarollo de apis Rest 
* Uso de Postman para consumo y pruebas de Apis. 
* Pruebas de estres con Jmeter. 
* Purebas de vulnerabilidad  y mejora de codigo con sonarqube.
* Desarrolo de proyecto con la teclonogia Spring boot. 
* Uso de base de datos como: MariaDb, PostgresSQL, AWS. 
* Capacidades para desarrollar en lenguajes como; Java, Php y JavaScript.
* Maquetación y construccion de paginas con HTML y Css. 

